GLOBAL cpuVendor
GLOBAL getSeconds
GLOBAL getMinutes
GLOBAL getHour

section .text
	
cpuVendor:
	push rbp
	mov rbp, rsp

	push rbx

	mov rax, 0
	cpuid


	mov [rdi], ebx
	mov [rdi + 4], edx
	mov [rdi + 8], ecx

	mov byte [rdi+13], 0

	mov rax, rdi

	pop rbx

	mov rsp, rbp
	pop rbp
	ret

%macro getRTC 1
	mov rax,0

    mov al,0Bh 
    out 70h,al ;asks clock to show state in 71h
    in al,71h ;gets clock state
    or al,4 ;4 represents the third byte which indicates BCD or time/date in binary
    out 71h, al ;we ask for the time in binary

    mov al, %1
    out 70h,al ;we ask for the ...
	in al,71h ;we get the ...

	ret
%endmacro

getSeconds:
	getRTC 0

getMinutes:
	getRTC 2

getHour:
	getRTC 4