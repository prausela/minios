global getKeyCode
global getKeyStatus

section .text

getKeyCode:
	push rbp
	mov rbp, rsp

	xor rax, rax   	; output register has data for system
	in al, 60h

	mov rsp, rbp
	pop rbp
	ret

getKeyStatus:
	push rbp
	mov rbp, rsp

	xor rax, rax   	
	in al, 64h

	mov rsp, rbp
	pop rbp
	ret	
