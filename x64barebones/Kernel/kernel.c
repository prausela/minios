#include <stdint.h>
#include <string.h>
#include <lib.h>
#include <moduleLoader.h>
#include <naiveConsole.h>
#include <videoDriver.h>
#include <keyboardDriver.h>
#include <idtLoader.h>

#define START 0

extern uint8_t text;
extern uint8_t rodata;
extern uint8_t data;
extern uint8_t bss;
extern uint8_t endOfKernelBinary;
extern uint8_t endOfKernel;

static const uint64_t PageSize = 0x1000;

static void * const sampleCodeModuleAddress = (void*)0x400000;
static void * const sampleDataModuleAddress = (void*)0x500000;

typedef int (*EntryPoint)();

void clearBSS(void * bssAddress, uint64_t bssSize)
{
	memset(bssAddress, 0, bssSize);
}

void * getStackBase()
{
	return (void*)(
		(uint64_t)&endOfKernel
		+ PageSize * 8				//The size of the stack itself, 32KiB
		- sizeof(uint64_t)			//Begin at the top of the stack
	);
}

void * initializeKernelBinary()
{
	char buffer[10];

	ncPrint("[x64BareBones]");
	ncNewline();

	ncPrint("CPU Vendor:");
	ncPrint(cpuVendor(buffer));
	ncNewline();

	ncPrint("[Loading modules]");
	ncNewline();
	void * moduleAddresses[] = {
		sampleCodeModuleAddress,
		sampleDataModuleAddress
	};

	loadModules(&endOfKernelBinary, moduleAddresses);
	ncPrint("[Done]");
	ncNewline();
	ncNewline();

	ncPrint("[Initializing kernel's binary]");
	ncNewline();

	clearBSS(&bss, &endOfKernel - &bss);

	ncPrint("  text: 0x");
	ncPrintHex((uint64_t)&text);
	ncNewline();
	ncPrint("  rodata: 0x");
	ncPrintHex((uint64_t)&rodata);
	ncNewline();
	ncPrint("  data: 0x");
	ncPrintHex((uint64_t)&data);
	ncNewline();
	ncPrint("  bss: 0x");
	ncPrintHex((uint64_t)&bss);
	ncNewline();

	ncPrint("[Done]");
	ncNewline();
	ncNewline();

    //ncPrintHex(startVideo());
  	//printScreenSize();
  	//renderString("Twinkle, twinkle, little star,\nHow I wonder what you are!\nUp above the world so high,\nLike a diamond in the sky.\nWhen this blazing sun is gone.\nWhen he nothing shines upon,\nThen you show your little light,\nTwinkle, twinkle, through the night.\nThen the traveller in the dark\nThanks you for your tiny spark;\nHe could not see where to go,\nIf you did not twinkle so.\nIn the dark blue sky you keep,\nAnd often through my curtains peep,\nFor you never shut your eye\nTill the sun is in the sky.\nAs your bright and tiny spark\nLights the traveller in the dark,\nThough I know not what you are,\nTwinkle, twinkle, little star.");
  	/*renderString("Hola");
  	scrollDown();

  	renderString("Chau chau ");
		printInt(getScreenHeight());
		renderString("  ");
		printInt(getScreenWidth());*/
  	/*Point point = {0, 0};
  	rgbColor color = {80, 255, 50};
  	for (int i = 0; i < 8; ++i)
  	{
  		point.y = i;
  		for (int j = 0; j < 3072; ++j)
  		{
  			point.x = j;
  			renderPixel(&point,&color);
  		}
  		if (i == 0)
  		{
  			color.green = 0;
	  		color.blue = 0;
	  		color.red = 255;
  		} else {
	  		color.green = 0;
	  		color.blue = 0;
	  		color.red = 0;
	  	}

  	}*/

	setLine(0);

  	load_idt();
	return getStackBase();
}

int main()
{
	//UserSpace
	((EntryPoint)sampleCodeModuleAddress)();

	/*ncPrint("[Kernel Main]");
	ncNewline();
	ncPrint("  Sample code module at 0x");
	ncPrintHex((uint64_t)sampleCodeModuleAddress);
	ncNewline();
	ncPrint("  Calling the sample code module returned: ");
	ncPrintHex(((EntryPoint)sampleCodeModuleAddress)());
	ncNewline();
	ncNewline();

	ncPrint("  Sample data module at 0x");
	ncPrintHex((uint64_t)sampleDataModuleAddress);
	ncNewline();
	ncPrint("  Sample data module contents: ");
	ncPrint((char*)sampleDataModuleAddress);
	ncNewline();

	ncPrint("[Finished]");*/



	//ncClear();
	while(1){
		//char c = getChar();
    	//ncPrintChar(getChar());
    	//getChar();
	}
	return 0;
}
