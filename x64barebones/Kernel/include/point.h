#ifndef _POINT_H_
#define _POINT_H_

#include <stdint.h>

typedef struct{
	uint16_t x;
	uint16_t y;
} Point;

#endif