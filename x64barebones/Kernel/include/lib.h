#ifndef LIB_H
#define LIB_H

#include <stdint.h>

#define NULL	0

void * memset(void * destination, int32_t character, uint64_t length);
void * memcpy(void * destination, const void * source, uint64_t length);

char *cpuVendor(char *result);

char getSeconds();
char getMinutes();
char getHour();

#endif
