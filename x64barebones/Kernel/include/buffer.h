#ifndef	_BUFFER_H_
#define	_BUFFER_H_

#define ZERO 0
#define BUFFER_SIZE 4096

typedef struct{
  char bufferContainer[BUFFER_SIZE];
  char* read;
  int countWrite;
  char* write;
  int countRead;
  int size;
}bufferCDT;

int write (bufferCDT * buffer, char* toCopy, int size);
int read (bufferCDT* buffer, char* string, int size);
int writeByte (bufferCDT* buffer, char byte, int size);
int readByte (bufferCDT* buffer, char* byte, int size);

#endif
