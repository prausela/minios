#include <stdint.h>
#include <videoDriver.h>
#include <font.h>

#define VESA_MODE_POINTER 0x5C00

/*


struct vbe_info_structure {
	char signature[4];	// must be "VESA" to indicate valid VBE support
	uint16_t version;			// VBE version; high byte is major version, low byte is minor version
	uint32_t oem;			// segment:offset pointer to OEM
	uint32_t capabilities;		// bitfield that describes card capabilities
	uint32_t video_modes;		// segment:offset pointer to list of supported video modes
	uint16_t video_memory;		// amount of video memory in 64KB blocks
	uint16_t software_rev;		// software revision
	uint32_t vendor;			// segment:offset to card vendor string
	uint32_t product_name;		// segment:offset to card model name
	uint32_t product_rev;		// segment:offset pointer to product revision
	char reserved[222];		// reserved for future expansion
	char oem_data[256];		// OEM BIOSes store their strings in this area
} __attribute__ ((packed));



*/

typedef struct {
	Point point;
} Cursor;

struct vbe_mode_info_structure * vbe_info = (struct vbe_mode_info_structure *) VESA_MODE_POINTER;

Cursor cursor = {{ 0 , 0 }};
rgbColor font_color = { 255 , 255 , 255 };
rgbColor background_color = { 0 , 0 , 0 };

static void renderBitMap(const char *bitmap);

void hexToRGB(rgbColor * rgb, uint64_t hex){
	rgb->blue = hex & 0xFF;
	hex >>= 8;
	rgb->green = hex & 0xFF;
	hex >>= 8;
	rgb->red = hex & 0xFF;
}


void renderPixel(Point* position, rgbColor* color){
	int pixelWidth = vbe_info->bpp / 8;
	unsigned char* pos = vbe_info->framebuffer + pixelWidth * position->x + vbe_info->pitch * position->y;
    pos[0] = color->blue;
	pos[1] = color->green;
	pos[2] = color->red;
}

void nextSpace(Cursor * cursor){
	cursor->point.x += FONT_WIDTH;
	if (cursor->point.x + FONT_WIDTH > vbe_info->width)	{
		scrollDown();
	}
}

void setColumn(int column){
	cursor.point.x = column * FONT_WIDTH;
}

void setLine(int line){
	cursor.point.y = (getLines() - line - 1) * FONT_HEIGHT;
}

int getColumns(){
	return vbe_info->width/FONT_WIDTH;
}

int getLines(){
	return vbe_info->height/FONT_HEIGHT;
}


void renderChar(const char c){
	switch(c){
		case '\n':
			scrollDown();
			return;
		case '\b':
		case '\t':
			break;
		default:
			renderBitMap(font8x8_basic[c]);
			nextSpace(&cursor);
	}	
}

void renderString(const char *s){
	while(*s != '\0'){
		renderChar(*s);
		s++;
	}
}


void scrollDown(){
	int pixelWidth = vbe_info->bpp / 8;
	
	memcpy(vbe_info->framebuffer, vbe_info->framebuffer + pixelWidth * vbe_info->width * FONT_HEIGHT
		, pixelWidth * vbe_info->width * (vbe_info->height - FONT_HEIGHT));
	cursor.point.x = 0;
	
}

void clearScreen(){
	int i, j;
	Point point = { 0 , 0 };
	for (i = 0; i < vbe_info->width; ++i){
		point.x = i;
		for (j = 0; j < vbe_info->height; ++j)
		{
			point.y = j;
			renderPixel(&point, &background_color);
		}
		
	}

	cursor.point.x = 0;
	cursor.point.y = 0;
}



void getScreenWidth(int * width){
	*width = vbe_info->width;
}

void getScreenHeight(int * height){
	*height = vbe_info->height;
}

static void renderBitMap(const char *bitmap) {
    int x,y;
    int set;
    char row;
    Point point;
    point.x = cursor.point.x;
    point.y = cursor.point.y;
    for (y=0; y < FONT_WIDTH; y++) {
    	point.y++;
    	row = bitmap[y];
        for (x=0; x < FONT_HEIGHT; x++) {
        	point.x++;
            set = (row & 1);
            row >>= 1;
            if (set){
            	renderPixel(&point, &font_color);
            }
        }
        point.x = cursor.point.x;

    }
}