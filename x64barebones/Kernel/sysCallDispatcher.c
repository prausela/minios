#include <stdint.h>
#include <keyboardDriver.h>
#include <videoDriver.h>
#include <lib.h>

void sysCallDispatcher(uint64_t sysCall, uint64_t param1, uint64_t param2, uint64_t param3) {
	switch (sysCall) {
		case 0:		//para leer un caracter del buffer
			getKey((char*)param1);
		case 1:		//para leer 'param2' caracteres del buffer. Se devuelve el puntero en param1 y en rax
			getKeys((char*) param1, (int) param2);
		case 2:		//guarda en el buffer un caracter
			saveKey();
			break;
		case 3:{
				Point point;
				point.x = param1;
				point.y = param2;
				rgbColor rgb;
				hexToRGB(&rgb, param3);
				renderPixel(&point, &rgb);
			}
			break;
		case 4:		//guarda segundos en rax
			{
				int s = getSeconds();
				int *to_s = (int *)param1;
				*to_s = s;
			}
			break;
		case 5:		//guarda minutos en rax
			{
				int m = getMinutes();
				int * to_m = (int*)param1;
				*to_m = m;
			}
			break;
		case 6:		//guarda horas en rax
			{
				int h = getHour();
				int *to_h = (int *)param1;
				*to_h = h;
			}
			break;
		case 7:
			getScreenWidth((int *) param1);
		case 8:
			getScreenHeight((int *) param1);
		case 9:
			beepOn();
			break;
		case 10:
			beepOff();
			break;
		case 11:
			renderChar(param1);
			break;
		case 12:
			renderString(param1);
			break;
		case 13:
			setLine(param1);
			break;
		case 14:
			{
				int l = getLines();
				int * to_l = (int *) param1;
				*to_l = l;
				
			}
			break;
	}
	return;
}
