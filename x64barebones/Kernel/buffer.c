#include <buffer.h>
#include <lib.h>

static int canRead (bufferCDT* buffer, int* size );
static int canWrite (bufferCDT* buffer, int* size);
static void increaseWrite(bufferCDT* buffer);
static void increaseRead (bufferCDT* buffer);

/*void initBuffer(bufferCDT * buffer){
  buffer->read = buffer->bufferContainer;
  buffer->write = buffer->bufferContainer;
  buffer->size = BUFFER_SIZE;
  buffer->countRead = 0;
  buffer->countWrite = 0;
}*/

int write (bufferCDT * buffer, char* toCopy, int size){
  int possibleSize;
  if(!canWrite(buffer, &possibleSize))
    return 0;
  if (possibleSize < size)
    return 0;
  for (int i =0; i<size; i++){
    *(buffer->write) = toCopy[i];
    increaseWrite(buffer);
  }
  return 1;
}

int read (bufferCDT * buffer, char* string, int size){
  int possibleSize;
  if (!canRead(buffer, &possibleSize))
    return 0;
  if (possibleSize < size)
    return 0;
  int i;
  for (i = 0; i<size; i++ ){
    string[i] = *(buffer->read);
    increaseRead(buffer);
  }
  return 1;
}

int writeByte (bufferCDT * buffer, char byte, int size){
  if (!canWrite(buffer, &size))
    return 0;
  *(buffer->write) = byte;
  increaseWrite(buffer);
  return 1;
}

int readByte (bufferCDT * buffer, char* byte, int size){
  if (!canRead(buffer, &size)){
    return 0;
  }
  *byte = *(buffer->read);
  increaseRead(buffer);
  return 1;

}

static
int canRead (bufferCDT * buffer, int* size ){   //devuelve 1 si puede leer o 0 sino y ademas en size la cantidad de caracteres posibles para leer
  *size = 0;
  if (buffer->read == NULL){
    buffer->read = buffer->bufferContainer;
  }
  if ((buffer->countRead) == (buffer->countWrite))
    return 0;
  if ((buffer->write) > (buffer->read))
    *size = (buffer->countWrite) - (buffer->countRead);
  else
    *size = buffer->size - ((buffer->countRead) + (buffer->countWrite));
  return 1;

}

static
int canWrite (bufferCDT * buffer, int* size){ //devuelve 0 si no puede escribir 1 si puede. Como el write escribe y avanza se puede escribir siempre y cuando el write no este atras del read asi no se igualan las posiciones y provoca que se pise todo el buffer.
  *size = 0;
  if (buffer->write == NULL){
    buffer->write = buffer->bufferContainer;
  }
  if ( ( (buffer->countWrite) == (buffer->countRead) - 1 ) || ( (buffer->countWrite) == buffer->size -1 && (buffer->countRead) == ZERO ) )
    return 0;
  if ((buffer->countWrite) == (buffer->countRead))
    *size = buffer->size -1;
  else if ((buffer->countWrite) < (buffer->countRead))
    *size = (buffer->countRead) - (buffer->countWrite) - 1;  //tengo que quedar una casilla atras del read para que no se pisen valores
  else
    *size = buffer->size - (buffer->countWrite) + buffer->countRead - 1;
  return 1;
}

static
void increaseWrite(bufferCDT * buffer){    //aumenta la posiciondel puntero write en 1
  if (buffer->countWrite == buffer->size){
    (buffer->write) = (buffer->bufferContainer);
    buffer->countWrite = 0;
  }
  else{
    ((buffer->write))++;
    (buffer->countWrite)++;
  }
}

static
void increaseRead (bufferCDT * buffer){    //aumenta el puntero read en 1
  if (buffer->countRead == buffer->size){
    (buffer->read) = (buffer->bufferContainer);
    buffer->countRead = 0;
  }
  else{
    (buffer->read)++;
    (buffer->countRead)++;
  }
}
