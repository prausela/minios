#include <exceptions.h>
#include <videoDriver.h>

void exceptionDispatcher(int exception, uint64_t * rsp) {
	if (exception == ZERO_EXCEPTION_ID) {
		zero_division(rsp);
	} else if (exception == INVALID_OPCODE_EXCEPTION_ID) {
		opcode(rsp);
	}
}

void zero_division(uint64_t * rsp) {
	ncNewline();
	char s = "Exception: division by zero";
	renderString(&s);
	ncNewline();
	printRegisters(rsp);
}

void opcode(uint64_t * rsp) {
	ncNewline();
	char s = "Exception: invalid opcode";
	renderString(&s);
	ncNewline();
	printRegisters(rsp);
	ncNewline();
}

void printRegisters(uint64_t * rsp) {
	renderString("RSP: ");
	//printHexa(rsp);
	ncNewline();
	for (int i = 0; i < REGISTERS; i++) {
		renderChar(registers[i]);
		renderChar(" ");
		//printSexa(rsp[i]);
		ncNewline();
	}
}
