#include <keyboardDriver.h>
#include <naiveConsole.h>
#include <lib.h>
#include <buffer.h>

#define TRUE                  1
#define	FALSE                 0
#define	SHIFT_LEFT	          0x2A
#define SHIFT_RIGHT	          0x36
#define	CAPS_LOCK             58
#define SHIFT_LEFT_RELEASED   0xAA
#define SHIFT_RIGHT_RELEASED  0xB6

#define IS_ALPHA(c)	( ((c)>='a' && (c)<='z') || ((c)>='A' && (c)<='Z') )

unsigned char kbdus[128] =
{
    0,  27, '1', '2', '3', '4', '5', '6', '7', '8',	/* 9 */
  '9', '0', '-', '=', '\b',	/* Backspace */
  '\t',			/* Tab */
  'q', 'w', 'e', 'r',	/* 19 */
  't', 'y', 'u', 'i', 'o', 'p', '[', ']', '\n',	/* Enter key */
    0,			/* 29   - Control */
  'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', ';',	/* 39 */
 '\'', '`',   0,		/* Left shift */
 '\\', 'z', 'x', 'c', 'v', 'b', 'n',			/* 49 */
  'm', ',', '.', '/',   0,				/* Right shift */
  '*',
    0,	/* Alt */
  ' ',	/* Space bar */
    0,	/* Caps lock */
    0,	/* 59 - F1 key ... > */
    0,   0,   0,   0,   0,   0,   0,   0,
    0,	/* < ... F10 */
    0,	/* 69 - Num lock*/
    0,	/* Scroll Lock */
    0,	/* Home key */
    0,	/* Up Arrow */
    0,	/* Page Up */
  '-',
    0,	/* Left Arrow */
    0,
    0,	/* Right Arrow */
  '+',
    0,	/* 79 - End key*/
    0,	/* Down Arrow */
    0,	/* Page Down */
    0,	/* Insert Key */
    0,	/* Delete Key */
    0,   0,   0,
    0,	/* F11 Key */
    0,	/* F12 Key */
    0,	/* All other keys are undefined */
};

unsigned char kbdusShift[128] =
{
    0,  27, '!', '@', '#', '$', '%', '^', '&', '*',
  '(', ')', '_', '+', 
    0, 
  '\t',      'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', '{', '}', 
    0, 
    0,
  'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', ':','"', '`',   0, 
 '|', 'Z', 'X', 'C', 'V', 'B', 'N', 'M', '<', '>', '?',   0,    
  '*',
    0,   
    ' ',
    0, 
    0,  
    0,   0,   0,   0,   0,   0,   0,   0,
    0,
    0, 
    0,
    0, 
    0, 
    0, 
  '-',
    0, 
    0,
    0, 
  '+',
    0, 
    0, 
    0, 
    0, 
    0, 
    0,   0,   0,
    0,  
    0, 
    0, 
};

bufferCDT buffer = { {0}, NULL, 0, NULL, 0, BUFFER_SIZE };

int upper = FALSE;
int shift = FALSE;

/*char getChar(){
	int beforeRead = read;
  read++;  
  return buffer[beforeRead];
}

char * getBuff(){
  buffer[write] = 0;
  ncPrint(buffer);
}*/

void saveKey(){
  char status = getKeyStatus();
  status=status&1;
	char scanCode = getKeyCode();
  if(status){
    if (scanCode <= 0) //Key release
    {
      unsigned char scanCode2 = scanCode & 0xFF;
      if(scanCode2 == SHIFT_RIGHT_RELEASED || scanCode2 == SHIFT_LEFT_RELEASED ){
        upper = !upper;
      }
      return;
    }
  	if(scanCode == CAPS_LOCK){
  		if(!upper)
  			upper = TRUE;
  		else
  			upper = FALSE;
      return;
  	} else if(scanCode == SHIFT_LEFT || scanCode == SHIFT_RIGHT){
      upper = !upper;
      return;
    }
  	else if(upper && IS_ALPHA(kbdus[scanCode])){
  		writeByte(&buffer, kbdusShift[scanCode], buffer.size);
  	} else if (!upper && IS_ALPHA(kbdus[scanCode])){	
      writeByte(&buffer, kbdus[scanCode], buffer.size);
    }
    else if(upper){
      writeByte(&buffer, kbdusShift[scanCode], buffer.size);
    }
    else{
      writeByte(&buffer, kbdus[scanCode], buffer.size);      
    }
  }
}

void getKey(char * key){
  if(readByte(&buffer, key, buffer.size)){
    return;
  }
  //renderChar(key);
  *key=-1;
  return;
}

char * getKeys(char *dest, int qty){
  if(read(&buffer, dest, qty)){
    return dest;
  }
  return NULL;
}


