#include <point.h>

void newPoint (Point * point, int x, int y){
  point->x = x;
  point->y = y;
}

int xComparator (Point * pointA, Point * pointB){
  if (pointA->x - pointB->x == 0)
    return 1;
  return 0;
}

int yComparator (Point * pointA, Point * pointB){
  if (pointA->y - pointB->y == 0)
    return 1;
  return 0;
}
