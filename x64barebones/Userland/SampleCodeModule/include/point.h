#ifndef _POINT_H_
#define _POINT_H_

typedef struct{
  int x;
  int y;
}Point;

void  newPoint (Point * point, int x, int y);

int xComparator (Point * pointA, Point * pointB);

int yComparator (Point * pointA, Point * pointB);

#endif
