#ifndef _SHELL_H_
#define _SHELL_H_

#include <command.h>

#define COMMAND_COUNT 5
extern const Command commands[COMMAND_COUNT];

int runShell();

#endif