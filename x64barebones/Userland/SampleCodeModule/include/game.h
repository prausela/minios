#ifndef _GAME_H
#define _GAME_H

#include <ball.h>

void game (int widht, int height);
void initAll(Rectangle* topEdge, Rectangle* bottomEdge, Player* playerRight, Player* playerLeft, Ball* ball, int widt, int height);
void initEdges(Rectangle* topEdge, Rectangle* bottomEdge, int widht, int height);
void initPlayers(Player * playerR, Player* playerL, int widht, int height);
void initBall (Ball* ball, int widht, int height);
void goal (Player* player1, Ball* ball, int* maxScore, Player* player2);
void showAll (Rectangle* topEdge, Rectangle* bottomEdge, Player* playerRight, Player* playerLeft, Ball* ball);
void resetAll (Ball* ball, Player* player1, Player* player2);


#endif
