#ifndef _SYS_CALLS_H_
#define _SYS_CALLS_H_

#include <stdint.h>

void sys_call(uint64_t sys_call, uint64_t param1, uint64_t param2, uint64_t param3);

#endif