#ifndef _MOVABLE_RECTANGLE_H_
#define _MOVABLE_RECTANGLE_H_

#include <rectangle.h>


typedef struct{
  Rectangle rectangle;
  Rectangle initPosition;
}MovableRectangle;

void newMovableRectangle (MovableRectangle * movableRectangle, Point initPosition, int width, int height);

void moveMovableRectangle (MovableRectangle * movableRectangle, int dir);

void resetMovableRectangle (MovableRectangle * movableRectangle);

void showMovableRectangle (MovableRectangle * movableRectangle);

void moveMovableRectangleBall (MovableRectangle* movableRectangle, int dir);

void hideMovableRectangle (MovableRectangle * movableRectangle);

int movableRectangleCollisionEdge (MovableRectangle * movableRectangle, Rectangle * rectangle);

int movableRectangleCollisionPlayer (MovableRectangle * movableRectangle1, MovableRectangle * movableRectanlge2);

#endif
