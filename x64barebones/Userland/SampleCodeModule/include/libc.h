#ifndef _LIBC_H_
#define	_LIBC_H_

#include <stdarg.h>
#include <stddef.h>

#define	BUFF_SIZE	100
#define IS_ALPHA(c)	((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z'))
#define IS_DIGIT(c)	(c >= '0' && c <= '9')

int getchar();
void putchar(char c);
//int getstr(char *buff);
void putstr(char *str);
void putnum(int num);
int strcpy(const char *src, const char *dest);
int printf(const char * fmt, ...);
int scanf(int n, const char *fmt, ...);

#endif