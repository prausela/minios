#ifndef _RECTANGLE_H_
#define _RECTANGLE_H_

#include <point.h>


typedef struct{
  Point top_left_corner;
  Point top_right_corner;
  Point bottom_right_corner;
  Point bottom_left_corner;
  int height;
  int width;
}Rectangle;



void newRectangle (Rectangle * rectangle, Point * point, int width, int height);

void showRectangle (Rectangle * rectangle);

void moveRectangle (Rectangle* rectangle, int dir);

void moveRectangleBall (Rectangle* rectangle, int dir);

void hideRectangle(Rectangle* rectangle);

int edgeCollision (Rectangle * ballRectangle, Rectangle * edgeRectangle);

int ballPlayerCollision (Rectangle * ballRectangle, Rectangle * playerRectangle);

void increaseHeight(Rectangle* rectangle);

void decreaseHeight(Rectangle* rectangle);

#endif
