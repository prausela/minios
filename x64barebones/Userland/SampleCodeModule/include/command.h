#ifndef _COMMAND_H_
#define _COMMAND_H_

typedef struct {
	char * name;
	int(*run)(void);
	char * help;	
} Command;

#endif