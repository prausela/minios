#ifndef _PLAYER_H_
#define _PAYER_H_

#include <movableRectangle.h>

//enum playerDirections {UP, DOWN};

typedef struct{
  int score;
  MovableRectangle movableRectangle;
}Player;

void  newPlayer (Player * player, Point initPoint, int width, int height);

void showPlayer( Player * player);

void movePlayer (Player * player, int* key);

void resetPosition (Player * Player);

void incScore (Player * player);

void resetScore (Player * player);

#endif
