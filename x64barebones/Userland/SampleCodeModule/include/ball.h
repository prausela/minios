#ifndef	_BALL_H_
#define	_BALL_H_

#include <movableRectangle.h>
#include <player.h>



//enum ballDirections {TOP_LEFT, TOP_RIGHT, DOWN_LEFT, DOWN_RIGHT};

typedef struct{
  int dir;
  MovableRectangle movableRectangle;
}Ball;

void newBall(Ball * ball, Point initPoint, int dim, int size);

void setBallDirection (Ball * ball, int dir);

void showBall(Ball * ball);

void hideBall (Ball* ball);

void moveBall (Ball * ball);

void resetBall (Ball * ball);

int ballCollisionEdge (Ball * ball, Rectangle * rectangle);

int ballCollisionPlayer (Ball * ball, Player * player);

#endif
