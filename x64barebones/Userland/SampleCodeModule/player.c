#include <player.h>

#define UP 1
#define DOWN -1

void  newPlayer (Player * player, Point initPoint, int width, int height){
  player->score = 0;
  newMovableRectangle(&player->movableRectangle, initPoint, width, height);
}

void showPlayer (Player * player){
  showMovableRectangle(&player->movableRectangle);
}

void movePlayer (Player * player, int* key){
  hideMovableRectangle(&player->movableRectangle);
  if( *key == 'w' || *key == 'i' ){
    int dir = UP;
    moveMovableRectangle(&player->movableRectangle, dir);
  }
  else if (*key == 's' || *key == 'k'){
    int dir = DOWN;
    moveMovableRectangle(&player->movableRectangle, dir);
  }
  showMovableRectangle(&hideMovableRectangle);

}

void resetPosition (Player * player){
  resetMovableRectangle(&player->movableRectangle);
}

void incScore (Player * player){
  player->score++;
}

void resetScore (Player * player){
    player->score = 0;
}
