#include <libc.h>
#include <sys_calls.h>

/*Sends formatted output to a string using an argument list.
**If successful, the total number of characters written is returned, 
**otherwise a negative number is returned.*/
static int vsprintf(const char *fmt, va_list arg);
static void putnumR(int num);
static int vsscanf(int n, const char *fmt, va_list arg);


int getchar(){
	char c = -1;
	while(c == -1){
		sys_call(0, &c, 0, 0);
	}
	return c;
}

void putchar(char c){
	sys_call(11, c, 0, 0);
}

/*int getstr(char *buff){
	int n = 0;
	while(*buff!='\0'){
		n++;
	}
	sys_call(1, buff, n, 0);

	return buff;
}*/

void putstr(char *str){
	sys_call(12, str, 0, 0);
}

void putnum(int num){
	if(num == 0){
		putchar('0');
		return;
	}
	if(num < 0){
		putchar('-');
		num *= -1;
	}
	putnumR(num);
}

static 
void putnumR(int num){
	if(num == 0)
		return;
	putnumR(num/10);
	putchar(num%10+'0');
}

/*int strcpy(const char *dest, char *src){
	int i;

	for(i=0; src[i] != '\0'; i++)
		dest[i]=src[i];

	dest[i]=0;

	return i;
}*/

static
int vsprintf(const char *fmt, va_list arg){
	int i, printed_chars;
	printed_chars = 0;

	for(i=0; fmt[i]!='\0'; i++){
		if(fmt[i] == '%'){
			switch(fmt[++i]){
				case 'c': putchar(va_arg(arg, int)); i++; break;
				case 'd': 	
				case 'i': putnum(va_arg(arg, int)); i++; break;
				case 's': putstr(va_arg(arg, char*)); i++; break;
			}
		}
		if(IS_DIGIT(fmt[i]))
			putnum(fmt[i]);
		else
			putchar(fmt[i]);
	}
	return printed_chars;
}

int printf(const char * fmt, ...){
	/*Declare arg_list, used to manipulating the argument list containing variable arguments of printf( ). 
	**The data type of the variable is va_list, a special type defined by <stdarg.h>.*/
	va_list arg_list;
	int done;

	if(fmt == NULL)
		return -1;

	va_start(arg_list, fmt);
	done = vsprintf(fmt, arg_list);
	/*After processing all the arguments, va_end() performs any necessary cleanup.*/
	va_end(arg_list);

	return done;
}

static
int vsscanf(int n, const char *fmt, va_list arg){
	int i=0, j=0, num=0;
	char buff[n];
	char c;

	while( (c=getchar())!='\n' && fmt[i]!='\0' && j<=n){
		if(fmt[i] == '%'){
			switch(fmt[i+1]){
				case 'c':{
					int *aux = va_arg(arg, int*);
					*aux = c;
					putchar(c);
					i++; }
					break;
				case 'd': 	
				case 'i':
					if(IS_DIGIT(c)){
						num *= 10 + (c-'0');	
						putchar(c);	}
					else{
						int *aux = va_arg(arg, int*);
						*aux = num;
						i++; }
					break;
				case 's':
					if(IS_ALPHA(c)){
						buff[j++]=c;
						putchar(c);	}
					else{
						char *aux = va_arg(arg, int*);
						buff[j]=0;
						*aux = buff;
						i++; }
					break;
				default: putchar(c);
			}
		}
	}
	return i;
}

int scanf(int n, const char *fmt, ...){
	va_list arg_list;
	int done;

	if(fmt == NULL)
		return -1;

	va_start(arg_list, fmt);
	done = vsscanf(n, fmt, arg_list);
	va_end(arg_list);

	return done;
}