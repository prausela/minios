#include <movableRectangle.h>


void newMovableRectangle (MovableRectangle * movableRectangle, Point initPoint, int width, int height){
  newRectangle(&movableRectangle->rectangle, &initPoint, width, height);
  movableRectangle->initPosition = movableRectangle->rectangle;
}

void moveMovableRectangle (MovableRectangle * movableRectangle, int dir){
  moveRectangle (&movableRectangle->rectangle, dir);
}

void resetMovableRectangle (MovableRectangle * movableRectangle){
  hideMovableRectangle(movableRectangle);
  movableRectangle->rectangle = movableRectangle->initPosition;
  showMovableRectangle(movableRectangle);

}

void showMovableRectangle (MovableRectangle * movableRectangle){
  showRectangle(&movableRectangle->rectangle);
}

void moveMovableRectangleBall (MovableRectangle* movableRectangle, int dir){
  moveRectangleBall(&movableRectangle->rectangle, dir);
}

void hideMovableRectangle (MovableRectangle * movableRectangle){
  hideRectangle(&movableRectangle->rectangle);
}

int movableRectangleCollisionEdge (MovableRectangle * movableRectangle, Rectangle * rectangle){
  return edgeCollision(&movableRectangle->rectangle, rectangle);
}

int movableRectangleCollisionPlayer (MovableRectangle * ballMovableRectangle, MovableRectangle * playerMovableRectangle){
   return ballPlayerCollision (&ballMovableRectangle->rectangle, &playerMovableRectangle->rectangle);
}
