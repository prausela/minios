/* sampleCodeModule.c */

#include <shell.h>
#include <sys_calls.h>
#include <game.h>

static
void putnum2R(int num){
	if(num == 0)
		return;
	putnum2R(num/10);
	sys_call(11, num%10 + '0', 0, 0);;
}

void putnum2(int num){
	if(num == 0){
		sys_call(11, '0', 0, 0);
		return;
	}
	if(num < 0){
		sys_call(11, '-', 0, 0);;
		num *= -1;
	}
	putnum2R(num);
}



int main() {
	int height, width;
	sys_call(7, &width, 0, 0);
	sys_call(8, &height, 0, 0);
	game(width, height);
	//runShell();
	//sys_call(1, 'a', 0, 0);

	//sys_call(11,'a',0,0);

	/*while(1){
		char c;
		sys_call(0, &c, 0, 0);
		if(c==-1){
			sys_call(11,'a', 0,0);
		}
		putnum2(c);
		sys_call(0, &c, 0, 0);
		putnum2(c);
	}*/
	sys_call(3, 20, 20, 0xFFFFFF);



	return 0;
}
