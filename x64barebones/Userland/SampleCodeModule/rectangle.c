#include <rectangle.h>

#define COLLISION 1
#define GOAL -1
#define NOCOLLISION 0
#define RECTANGLE_MOVEMENT 10
#define UP 1
#define DOWN -1

#define MOVE_BALL_X 1
#define MOVE_BALL_Y 0
#define TOP_LEFT 0
#define TOP_RIGHT 1
#define DOWN_LEFT 2
#define DOWN_RIGHT 3

#define UP 1
#define DOWN -1

void putnum(int);

void newRectangle (Rectangle * rectangle, Point * point, int width, int height){
  rectangle->width = width;
  rectangle->height = height;
  newPoint (&rectangle->top_left_corner, point->x, point->y);
  newPoint (&rectangle->top_right_corner, (point->x) + width, point->y);
  newPoint (&rectangle->bottom_left_corner, point->x, (point->y) + height);
  newPoint (&rectangle->bottom_right_corner, (point->x) + width, (point->y) + height);
}

void showRectangle (Rectangle * rectangle){
  //putnum(rectangle->top_left_corner.y);
  //putnum(rectangle->top_left_corner.y);
  for (int height = rectangle->top_left_corner.y ; height <= rectangle->bottom_right_corner.y; height++){
    for(int widht = rectangle->top_left_corner.x; widht <= rectangle->bottom_right_corner.x; widht++){
      //putchar('a');
      sys_call(3, widht, height, 0xFFFFFF);
    }
  }

}

void moveRectangle (Rectangle* rectangle, int dir){
  hideRectangle(rectangle);
  if (dir == UP){
    decreaseHeight(rectangle);
  }
  else if (dir == DOWN)
    increaseHeight(rectangle);

  showRectangle(rectangle);
}

void moveRectangleBall (Rectangle* rectangle, int dir){
  hideRectangle(rectangle);
  if (dir == TOP_LEFT){
    rectangle->top_left_corner.x -= MOVE_BALL_X;
    rectangle->bottom_left_corner.x -= MOVE_BALL_X;
    rectangle->bottom_right_corner.x -= MOVE_BALL_X;
    rectangle->top_right_corner.x -= MOVE_BALL_X;
    rectangle->top_left_corner.y -= MOVE_BALL_Y;
    rectangle->bottom_left_corner.y -= MOVE_BALL_Y;
    rectangle->bottom_right_corner.y -= MOVE_BALL_Y;
    rectangle->top_right_corner.y -= MOVE_BALL_Y;
    }
  else if(dir == TOP_RIGHT){
    rectangle->top_left_corner.x += MOVE_BALL_X;
    rectangle->bottom_left_corner.x += MOVE_BALL_X;
    rectangle->bottom_right_corner.x += MOVE_BALL_X;
    rectangle->top_right_corner.x += MOVE_BALL_X;
    rectangle->top_left_corner.y -= MOVE_BALL_Y;
    rectangle->bottom_left_corner.y -= MOVE_BALL_Y;
    rectangle->bottom_right_corner.y -= MOVE_BALL_Y;
    rectangle->top_right_corner.y -= MOVE_BALL_Y;
  }
  else if (dir == DOWN_LEFT){
    rectangle->top_left_corner.x -= MOVE_BALL_X;
    rectangle->bottom_left_corner.x -= MOVE_BALL_X;
    rectangle->bottom_right_corner.x -= MOVE_BALL_X;
    rectangle->top_right_corner.x -= MOVE_BALL_X;
    rectangle->top_left_corner.y += MOVE_BALL_Y;
    rectangle->bottom_left_corner.y += MOVE_BALL_Y;
    rectangle->bottom_right_corner.y += MOVE_BALL_Y;
    rectangle->top_right_corner.y += MOVE_BALL_Y;
  }
  else if(dir == DOWN_RIGHT){
    rectangle->top_left_corner.x += MOVE_BALL_X;
    rectangle->bottom_left_corner.x += MOVE_BALL_X;
    rectangle->bottom_right_corner.x += MOVE_BALL_X;
    rectangle->top_right_corner.x += MOVE_BALL_X;
    rectangle->top_left_corner.y += MOVE_BALL_Y;
    rectangle->bottom_left_corner.y += MOVE_BALL_Y;
    rectangle->bottom_right_corner.y += MOVE_BALL_Y;
    rectangle->top_right_corner.y += MOVE_BALL_Y;
  }
  showRectangle(rectangle);

}

void hideRectangle(Rectangle * rectangle){
  for (int height = rectangle->top_left_corner.y ; height <= rectangle->bottom_right_corner.y; height++){
    for(int widht = rectangle->top_left_corner.x; widht <= rectangle->bottom_right_corner.x; widht++){
      //putchar('a');
      sys_call(3, widht, height, 0x000000);
    }
  }
}

int edgeCollision (Rectangle * ballRectangle, Rectangle * edgeRectangle){
  if (yComparator(&ballRectangle->top_left_corner, &edgeRectangle->bottom_right_corner) || yComparator(&ballRectangle->bottom_right_corner, &edgeRectangle->top_left_corner))
    return COLLISION;
  return NOCOLLISION;
}

int ballPlayerCollision (Rectangle * ballRectangle, Rectangle * playerRectangle){
  if (xComparator(&ballRectangle->top_left_corner, &playerRectangle->bottom_right_corner) || xComparator(&ballRectangle->bottom_right_corner, &playerRectangle->top_left_corner)){
    for (int ball_y = &ballRectangle->top_left_corner; ball_y <= &ballRectangle->bottom_right_corner; ball_y++){
      for (int player_y = &playerRectangle->top_left_corner; player_y <= &playerRectangle->bottom_right_corner; player_y++){
        if (ball_y - player_y)
          return COLLISION;
      }
    }
    return GOAL;
  }
  return NOCOLLISION;
}

void increaseHeight(Rectangle* rectangle){
  rectangle->top_left_corner.y += RECTANGLE_MOVEMENT;
  rectangle->top_right_corner.y += RECTANGLE_MOVEMENT;
  rectangle->bottom_left_corner.y += RECTANGLE_MOVEMENT;
  rectangle->bottom_right_corner.y += RECTANGLE_MOVEMENT;
}

void decreaseHeight(Rectangle* rectangle){
  rectangle->top_left_corner.y -= RECTANGLE_MOVEMENT;
  rectangle->top_right_corner.y -= RECTANGLE_MOVEMENT;
  rectangle->bottom_left_corner.y -= RECTANGLE_MOVEMENT;
  rectangle->bottom_right_corner.y -= RECTANGLE_MOVEMENT;
}
