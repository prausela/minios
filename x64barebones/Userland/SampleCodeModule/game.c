#include <game.h>
#include <libc.h>

#define WIDHT_PLAYER 4
#define HEIGHT_PLAYER 120
#define WIDHT_EDGE 1000
#define HEIGHT_EDGE 8
#define WIDHT_BALL 6
#define SEPARATION_SIZE_X 10
#define SEPARATION_SIZE_Y 60
#define GOAL -1
#define COLLISION 1


#define TOP_LEFT 0
#define TOP_RIGHT 1
#define DOWN_LEFT 2
#define DOWN_RIGHT 3

void putnum(int);

void game (int widht, int height){
  int key;
  int maxScore = 0;
  int scoreOnLeft;
  int scoreOnRight;
  Rectangle topEdge;
  Rectangle bottomEdge;
  Player playerRight;
  Player playerLeft;
  Ball ball;


  initAll (&topEdge, &bottomEdge, &playerRight, &playerLeft, &ball, widht, height);
  putnum(topEdge.top_left_corner.y);
  putnum(topEdge.bottom_right_corner.y);


  showAll(&topEdge, &bottomEdge, &playerLeft, &playerRight, &ball);

  while (maxScore < 5) {

    moveBall(&ball);
    scoreOnRight = ballCollisionPlayer (&ball, &playerRight);
    scoreOnLeft = ballCollisionPlayer (&ball, &playerLeft);


    //showPlayer(&playerRight);
    //showPlayer(&playerLeft);


    for (int i=0; i< 1000000; i++); //velocidad de pelota


    key  = getchar();

    //sys_call(0, &key, 0, 0);

      if (key == 'w' || key == 's' )
        movePlayer(&playerLeft, &key);
      else if (key == 'i' || key == 'k')
        movePlayer(&playerRight, &key);




    if (ballCollisionEdge(&ball, &topEdge) || ballCollisionEdge(&ball, &bottomEdge)){
      setBallDirection(&ball, (ball.dir +2)%4);
    }
    else if (scoreOnLeft == COLLISION){
      setBallDirection(&ball, (ball.dir +1));

    }
    else if (scoreOnRight == COLLISION){

      setBallDirection(&ball, (ball.dir-1));
    }
    else if (scoreOnLeft == GOAL){
      goal(&playerRight, &ball, &maxScore, &playerLeft);
    }
    else if (scoreOnRight == GOAL)
      goal(&playerLeft, &ball, &maxScore, &playerRight);


  }

}


void initAll(Rectangle* topEdge, Rectangle* bottomEdge, Player* playerRight, Player* playerLeft, Ball* ball, int widht, int height){
  initEdges(topEdge, bottomEdge, widht, height);
  initPlayers(playerRight, playerLeft, widht, height);
  initBall(ball, widht, height);
}

void initEdges (Rectangle* topEdge, Rectangle* bottomEdge, int widht, int height){
  int x = SEPARATION_SIZE_X;
  int y = SEPARATION_SIZE_Y;
  Point point;
  newPoint(&point, x, y);
  newRectangle(topEdge, &point, WIDHT_EDGE, HEIGHT_EDGE);   //puede generar error a futuro
  y = height - HEIGHT_EDGE;
  newPoint(&point, x, y);
  newRectangle(bottomEdge, &point, WIDHT_EDGE, HEIGHT_EDGE);  //puede generar error a futuro
}


void initPlayers (Player* playerRight, Player* playerLeft, int widht, int height){
  int x = SEPARATION_SIZE_X;
  int y = (height/2) - HEIGHT_PLAYER/2;
  Point pointPL;
  newPoint(&pointPL, x, y);
  newPlayer(playerLeft, pointPL, WIDHT_PLAYER, HEIGHT_PLAYER);

  x= WIDHT_EDGE - WIDHT_PLAYER - SEPARATION_SIZE_X;
  Point pointPR;
  newPoint(&pointPR, x, y);
  newPlayer(playerRight, pointPR, WIDHT_PLAYER, HEIGHT_PLAYER);
}

void initBall (Ball * ball, int widht, int height){
  int x = WIDHT_EDGE-50;
  int y = height/2 + WIDHT_BALL/2;
  Point point;
  newPoint(&point, x, y);
  newBall(ball, point, 3, WIDHT_BALL);  //1 es TOP_RIGHT
}

void goal (Player* player1, Ball* ball, int* maxScore, Player* player2){
  for (int i = 0; i< 10000; i++){
    sys_call(9, 0, 0, 0, 0);
  }
  sys_call(10, 0, 0, 0, 0);

  player1->score++;
  if (*maxScore < player1->score)
    *maxScore = player1->score;
  resetAll (ball, player1, player2);
}

void resetAll (Ball* ball, Player* player1, Player* player2){
  resetBall(ball);
  resetPosition(player1);
  resetPosition(player2);
}

void showAll (Rectangle* topEdge, Rectangle* bottomEdge, Player* playerRight, Player* playerLeft, Ball* ball){
  showRectangle(topEdge);
  showPlayer(playerRight);
  showPlayer(playerLeft);
  showRectangle(bottomEdge);
  showBall(ball);
}
