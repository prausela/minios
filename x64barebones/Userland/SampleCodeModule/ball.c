#include <ball.h>



void newBall(Ball * ball, Point initPosition, int dir, int size){
  ball->dir = dir;
  newMovableRectangle(&ball->movableRectangle, initPosition, size, size);
}

void showBall (Ball * ball){
  showMovableRectangle(&ball->movableRectangle);
}

void setBallDirection (Ball * ball, int dir){
  ball->dir = dir;
}

void moveBall (Ball * ball){
  moveMovableRectangleBall(&ball->movableRectangle, ball->dir);
}

void resetBall (Ball * ball){ //reset ballposition to restart game or after goal
  resetMovableRectangle(&ball->movableRectangle);
}

void hideBall (Ball* ball){
  hideMovableRectangle(&ball->movableRectangle);
}

int ballCollisionEdge (Ball * ball, Rectangle * rectangle){ //return 1 to collision, so change direction. return o to nocollision
  return movableRectangleCollisionEdge(&ball->movableRectangle, rectangle);
}

int ballCollisionPlayer (Ball * ball, Player * player){ //return 1 to COLLISION, so change collsion to oposite. return -1 to goal, return 0 to nocollision
  return movableRectangleCollisionPlayer (&ball->movableRectangle, &player->movableRectangle);
}
